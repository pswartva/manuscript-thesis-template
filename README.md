# Modèle pour manuscrit de thèse

Ce modèle pour manuscrit de thèse est hérité de Nicolas Denoyelle, Valentin
Honoré et Nicolas Vidal. Il a été "nettoyé" pour ne contenir que le strict
minimum, le contenu supposé commun à tous les manuscrits de thèses. La page de
garde correspond *à peu près* au modèle fourni sur ADUM.

Ce dépôt fournit tous les éléments nécessaires pour démarrer sereinement la
rédaction de son manuscrit de thèse.

N'hésitez pas à envoyer d'éventuels correctifs.

Bon courage !


# Utilisation

Simplement lancer :
```
make
```
