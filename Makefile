all: main

main: main.pdf

main.glo: acronymes.tex
	pdflatex main.tex
	makeglossaries main

main.pdf: *.tex *.bib main.glo
	rubber --unsafe --pdf main.tex

clean:
	rm -f *.aux *.fdb_latexmk *.fls *.log main.pdf *.synctex.gz
